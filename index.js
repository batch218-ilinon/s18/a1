function addition(num1, num2){
		console.log(num1 + num2);
	}

	console.log("Displayed sum of 5 and 15");
	addition(5, 15);

	function subtraction(num1, num2){
		console.log(num1 - num2);
	}

	console.log("Displayed difference of 20 and 5");
	subtraction(20, 5);

	// 2.
	function multiplication(num1, num2){
		return (num1 * num2);
	}

	console.log("The product of 50 and 10");
	let product = multiplication(50, 10);
	console.log(product);

	function division(num1, num2){
		return (num1 / num2);
	}

	console.log("The quotient of 50 and 10");
	let quotient = division(50, 10);
	console.log(quotient);

	// 3.
	function areaOfCircle(rad){
		return Math.PI * rad**2;
	}
	console.log("The result of getting the area of a circle with 15 raidus:")
	let circleArea = parseFloat(areaOfCircle(15).toFixed(2));
	console.log(circleArea);
	
	//  4.
	function average(num1, num2, num3, num4){
		return (num1 + num2 + num3 + num4) / 4;
	}
	console.log("The average of 20,40,60,80:")
	let averageVar = average(20, 40, 60, 80);
	console.log(averageVar);

	// 5.
	function passingScore(num1, num2){
		let isPassed = (num1 / num2) * 100 > 75;
		return isPassed;
	}
	console.log("Is 38/50 a passing score?");
	let isPassingScore = passingScore(38, 50);
	console.log(isPassingScore);